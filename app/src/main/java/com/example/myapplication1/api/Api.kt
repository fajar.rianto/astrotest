package com.example.myapplication1.api

import com.example.myapplication1.data.model.UserResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface Api {
    @GET("search/users")
    @Headers("Auhorization: token aa22cb9bfab3d439f33e98d011b7cefd7b88ca05 ")
    fun getSearchUsers(
        @Query("q") query: String
    ): Call<UserResponse>


}